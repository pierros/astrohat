# Astrohat

A RaspberryPi4 compatible hat for all your astronomy equipment. Features include:

* 6 12V controllable outputs @ 3A each with current monitoring (2 PWM controllable for dew heaters)
* Temperature, Humidity and Pressure sensor port (external module)
* 1 adjustable 6-12 V output
* Port for serial communication and power to external device (GPS)y
* Automotive grade electonics and design

![Astrohat](https://gitlab.com/pierros/astrohat/-/raw/master/astrohat.png)

# Contribute

Schematic and PCB are designed on KiCAD v5.1

# License

Licensed under the [CERN OHLv1.2](LICENSE) and [CC-BY-SA v4](https://creativecommons.org/licenses/by-sa/4.0/) for the Documentation.
2019-2021 Pierros Papadeas.
